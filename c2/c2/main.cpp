//
//  main.cpp
//  c2
//
//  Created by Oskar Gargas on 24.11.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

class Set {
    public:
    Set() {
        p = this;
        rank = 1;
    }

    int add(Set *x) {
        if (x == this) return rank;
        return link(findSet(this), findSet(x));
    }

    int getRank() {
        return findSet(this)->rank;
    }

    private:
    Set *p;
    int rank;

    int link(Set *x, Set *y);

    Set *findSet(Set *x);

};

int readLine(int & left, int & right);

int main(int argc, const char *argv[]) {
    unsigned int testsNumber = 0;
    int computersNumber = 0;
    fscanf(stdin, "%u", &testsNumber);

    while (testsNumber--) {
        fscanf(stdin, "%d", &computersNumber);

        int networksNumber = computersNumber;
        int biggestNetwork = 1;
        int connectionLeft = -2, connectionRight = -1;

        Set *computersTable = new Set[computersNumber];

        int readLineRet;
        while ((readLineRet = readLine(connectionLeft, connectionRight)) != 0) {
            if (readLineRet == 1) {
                int rankBeforeUnion = computersTable[connectionLeft].getRank();

                //Union
                int addRet = computersTable[connectionLeft].add(&(computersTable[connectionRight]));
                if (biggestNetwork < addRet)
                    biggestNetwork = addRet;

                if (computersTable[connectionLeft].getRank() != rankBeforeUnion)
                    networksNumber--;
            } else {
                fprintf(stdout, "%d %d\n", networksNumber, biggestNetwork);
            }
        }

        delete[] computersTable;
    }

    return 0;
}

int readLine(int & left, int & right) {
    fscanf(stdin, "%d %d", &left, &right);

    if (left == -1 && right == -1)
        return -1;
    else if (left == -2 && right == -2)
        return 0;

    return 1;
}

int Set::link(Set *x, Set *y) {
    if (x == y) return x->rank;

    int ret;
    if (x->rank > y->rank) {
        y->p = x;
        x->rank += y->rank;
        ret = x->rank;
    } else {
        x->p = y;
        y->rank += x->rank;
        ret = y->rank;
    }

    return ret;
}

Set *Set::findSet(Set *x) {
    Set *root = x;

    while (root->p != root)
        root = root->p;

    if (x != root || x->p != root) {
        Set *tmp = x;
        Set *tmp2;

        while (tmp->p != tmp) {
            tmp2 = tmp->p;
            tmp->p = root;
            tmp = tmp2;
        }
    }

    return root;
}