//
//  main.cpp
//  w1
//
//  Created by Oskar Gargas on 22.10.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

int main(void) {
	unsigned long long left, right, number;
	bool palindrom;
	
	while (scanf("%llu", &number) == 1) {
		right = 1ll;
		
		if(!(number & right)) {
			printf("NIE\n");
			continue;
		}
		
		left = 1ll << 63;
		
		while(!(left & number))
			left = left >> 1;
        
		palindrom = true;
		
		while(left > right) {
			if( ((left & number) != 0) == ((right & number) != 0) ) {
				left = left >> 1;
				right = right << 1;
			} else {
				palindrom = false;
				break;
			}
		}
		
		if (palindrom)
			printf("TAK\n");
		else
			printf("NIE\n");
	}
	
	return 0;
}
