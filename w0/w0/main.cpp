//
//  main.cpp
//  w0
//
//  Created by Oskar Gargas on 10.10.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

inline unsigned int *newMatrix(short, short);
inline void deleteMatrix(unsigned int*, short);
inline void printMatrix(unsigned int*, short, short);
inline void printTMatrix(unsigned int*, short, short);
//void transposeMatrix(unsigned int *tab, short &r, short &c);

int main(void) {
	short r, c;
	
	scanf("%hd %hd", &r, &c);
	
	unsigned int *tab = newMatrix(r, c);
	
	for(short i = 0; i < r; i++)
        for(short j = 0; j < c; j++)
            scanf("%u", &tab[i * c + j]);
	
	printTMatrix(tab, r, c);
    
	deleteMatrix(tab, r);
	
	return 0;
}

inline unsigned int *newMatrix(short r, short c) {
	return new unsigned int[r * c];
}

inline void deleteMatrix(unsigned int *tab, short r) {
	delete[] tab;
}

inline void printMatrix(unsigned int *tab, short r, short c) {
	printf("%hd %hd\n", r, c);
	for(short i = 0; i < r; i++) {
		for(short j = 0; j < c; j++)
			printf("%u ", tab[i * c + j]);
		printf("\n");
	}
}

inline void printTMatrix(unsigned int *tab, short r, short c) {
	printf("%hd %hd\n", c, r);
	for(short i = 0; i < c; i++) {
		for(short j = 0; j < r; j++)
			printf("%u ", tab[j * c + i]);
		printf("\n");
	}
}

/* 
 void transposeMatrix(unsigned int *tab, short &r, short &c) {
 short tR = c;
 short tC = r;
 
 unsigned int tmp;
 
 
 //	1  2  3  4	0*4+0	0*4+1	0*4+2	0*4+3
 //	5  6  7  8	1*4+0	1*4+1	1*4+2	1*4+3
 //	9 10 11 12	2*4+0	2*4+1	2*4+2	2*4+3
 //	
 //	1 5  9	0*3+0	1*3+0	2*3+0
 //	2 6 10	0*3+1	1*3+1	2*3+1
 //	3 7 11	0*3+2	1*3+2	2*3+2
 //	4 8 12	0*3+3	1*3+3	2*3+3
 
 
 //	for(short i = 1; i < r * c / 2; i++) {
 //		tmp = tab[i];
 //		tab[i] = tab[i + c];
 //		tab[i + c] = tmp;
 //	}
 //	
 for(short i = 0; i < r; i++)
 for(short j = i + 1; j < c; j++) {
 tmp = tab[i * c + j];
 tab[i * c + j] = tab[j * r + i];
 tab[j * r + i] = tmp;
 //		printMatrix(tab, tR, tC);
 //		printf("\n");
 }	
 
 r = tR;
 c = tC;
 } */
