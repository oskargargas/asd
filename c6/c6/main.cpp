//
//  main.cpp
//  c6
//
//  Created by Oskar Gargas on 15.01.2012.
//  Copyright (c) 2012 PJWSTK. All rights reserved.
//

#include <cstdio>

#define BIALY -1
#define SZARY 2

template <class T>
class FIFO {
    public:
    FIFO() {
        FIFO(1000000);
    }
    
    FIFO(unsigned int n) {
        elements = 0;
        tail = 0;
        head = 0;
        size = n;
        q = new T[n];
    }
    
    ~FIFO() {
        delete[] q;
    }

    void enqueue(T el) {
        q[tail++] = el;
        elements++;

        if(tail == size)
            tail = 0;
    }

    T dequeue() {
        T x = q[head++];
        elements--;

        if (head == size)
            head = 0;

        return x;
    }

    bool empty() {
        return !elements;
    }

    private:
    T *q;
    unsigned int head;
    unsigned int tail;
    unsigned int elements;
    unsigned int size;
};

struct Edge {
    unsigned int toNode;
    Edge *next;

    Edge() {
        toNode = 0;
        next = NULL;
    }
};

#define eMaxVal 32000000
Edge eMemPool[eMaxVal];
unsigned int eMemCounter = 0;

class Graph {
    public:
    Graph(unsigned int nodesNumber) {
        this->nodesNum = nodesNumber;
        this->sumaWag = 0;
        this->nodes = new Edge*[nodesNum];

        for (unsigned int i = 0; i < nodesNumber; i++)
            this->nodes[i] = NULL;
    }

    ~Graph() {
        delete[] this->nodes;
    }

    void addEdge(unsigned int fromNode, unsigned int toNode) {
        Edge *e = &eMemPool[eMemCounter++];

        e->toNode = toNode;
        e->next = nodes[fromNode];
        nodes[fromNode] = e;

        e = &eMemPool[eMemCounter++];

        e->toNode = fromNode;
        e->next = nodes[toNode];
        nodes[toNode] = e;
    }

    unsigned int getSumaWag() {
        return sumaWag;
    }

    unsigned int getNodesNum() {
        return nodesNum;
    }

    unsigned int nodesNum;
    unsigned int sumaWag;
    Edge **nodes;

};


unsigned int find(unsigned int *forest, unsigned int node) {
    unsigned int root = node;

    while (root != forest[root])
            root = forest[root];

    if (node != root || forest[node] != root) {
        unsigned int tmp = node;
        unsigned int tmp2;

        while (forest[tmp] != tmp) {
            tmp2 = forest[tmp];
            forest[tmp] = root;
            tmp = tmp2;
        }
    }

    return forest[node];
}

void link(unsigned int *cityForest, unsigned int a, unsigned int b) {
    cityForest[b] = a;
}

void unionSets (unsigned int *cityForest, unsigned int a, unsigned int b) {
    link(cityForest, find(cityForest, a), find(cityForest, b));
}

void performTest(Graph *g, unsigned int *cywilizacje, int iloscCywilizacji) {
    
    int *kolor = new int[g->getNodesNum()];
    int *odleglosc = new int[g->getNodesNum()];

    unsigned int *uklady = new unsigned int[g->getNodesNum()];
        for (unsigned int i = 0; i < g->getNodesNum(); i++)
            uklady[i] = i;

    for (unsigned int i = 0; i < g->getNodesNum(); i++) {
        kolor[i] = BIALY;
        odleglosc[i] = 0;
    }

    FIFO <unsigned int> f(g->nodesNum + 10);

    for (int i = 0; i < iloscCywilizacji; i++) {
        kolor[cywilizacje[i]] = cywilizacje[i];
        f.enqueue(cywilizacje[i]);
    }
    
    while (!f.empty()) {
        unsigned int u = f.dequeue();
        
        Edge *tmp = g->nodes[u];
        while (tmp != NULL) {
            if (kolor[tmp->toNode] == BIALY) {
                //Ekspansja cywilizacji do nie zbadanego sektora
                
                odleglosc[tmp->toNode] = odleglosc[u] + 1;
                kolor[tmp->toNode] = kolor[u];

                unionSets(uklady, u, tmp->toNode);

                f.enqueue(tmp->toNode);
            } else if (find(uklady, u) != find(uklady, tmp->toNode)) {
                //Spotkanie dwu cywilizacji
                fprintf(stdout, "%d ", odleglosc[u] + 1);
                unionSets(uklady, u, tmp->toNode);
            }

            tmp = tmp->next;
        }
    }

    delete[] kolor;
    delete[] odleglosc;
}

int main (int argc, const char * argv[]) {

    unsigned int iloscMiast, iloscWezlow, iloscKrawedzi;

    fscanf(stdin, "%u", &iloscMiast);

    while (iloscMiast--) {
        eMemCounter = 0;
        fscanf(stdin, "%u %u", &iloscWezlow, &iloscKrawedzi);

        Graph *graph = new Graph(iloscWezlow);

        while (iloscKrawedzi--) {
            unsigned int fromNode, toNode;
            fscanf(stdin, "%u %u", &fromNode, &toNode);

            graph->addEdge(fromNode, toNode);
        }

        unsigned int iloscCywilizacji;
        fscanf(stdin, "%u", &iloscCywilizacji);

        unsigned int *cywilizacje = new unsigned int [iloscCywilizacji];
        
        for (unsigned int i = 0; i < iloscCywilizacji; i++) {
            unsigned int tmp;
            fscanf(stdin, "%u", &tmp);
            cywilizacje[i] = tmp;
        }
        
        performTest(graph, cywilizacje, iloscCywilizacji);

        delete graph;
        delete[] cywilizacje;
        fprintf(stdout, "\n");
    }

    return 0;
}
