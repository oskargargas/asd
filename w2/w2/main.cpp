//
//  main.cpp
//  w2
//
//  Created by Oskar Gargas on 22.10.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

int main(int argc, char *argv[]) {
	short int n;
	scanf("%hd", &n);
    //	n = 10000;
	
	short *tab = new short[n - 1];
	for(short i = 0, in = 2; i < n - 1 ; i++, in++)
		tab[i] = in;
	
	for(short i = 0; i * i < n; i++) {
		if(!tab[i]) continue;
        
		for(short ii = i + 1; ii < n - 1; ii++) {
			if(!tab[ii]) continue;
            
			if(tab[ii] % tab[i] == 0)
                tab[ii] = 0;
		}
	}
	
	for(short i = 0; i < n - 1; i++)
		if(tab[i])
			printf("%hd\n", tab[i]);
    
	delete[] tab;
    
	return 0;
}
