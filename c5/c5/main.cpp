//
//  main.cpp
//  c5
//
//  Created by Oskar Gargas on 15.01.2012.
//  Copyright (c) 2012 PJWSTK. All rights reserved.
//

#include <cstdio>

#define BIALY 1
#define SZARY 2
#define CZARNY 3

template <class T>
class FIFO {
    public:
    FIFO() {
        FIFO(1000000);
    }
    
    FIFO(unsigned int n) {
        elements = 0;
        tail = 0;
        head = 0;
        size = n;
        q = new T[n];
    }
    
    ~FIFO() {
        delete[] q;
    }

    void enqueue(T el) {
        q[tail++] = el;
        elements++;

        if(tail == size)
            tail = 0;
    }

    T dequeue() {
        T x = q[head++];
        elements--;

        if (head == size)
            head = 0;

        return x;
    }

    bool empty() {
        return !elements;
    }

    private:
    T *q;
    unsigned int head;
    unsigned int tail;
    unsigned int elements;
    unsigned int size;
};

struct Edge {
    unsigned int toNode;
    Edge *next;

    Edge() {
        toNode = 0;
        next = NULL;
    }
};

#define eMaxVal 32000000
Edge eMemPool[eMaxVal];
unsigned int eMemCounter = 0;

class Graph {
    public:
    Graph(unsigned int nodesNumber) {
        this->nodesNum = nodesNumber;
        this->sumaWag = 0;
        this->nodes = new Edge*[nodesNum];

        for (unsigned int i = 0; i < nodesNumber; i++)
            this->nodes[i] = NULL;
    }

    ~Graph() {
        delete[] this->nodes;
    }

    void addEdge(unsigned int fromNode, unsigned int toNode) {
        Edge *e = &eMemPool[eMemCounter++];

        e->toNode = toNode;
        e->next = nodes[fromNode];
        nodes[fromNode] = e;

        e = &eMemPool[eMemCounter++];

        e->toNode = fromNode;
        e->next = nodes[toNode];
        nodes[toNode] = e;
    }

    unsigned int getSumaWag() {
        return sumaWag;
    }

    unsigned int getNodesNum() {
        return nodesNum;
    }

    unsigned int nodesNum;
    unsigned int sumaWag;
    Edge **nodes;

};

int bfs_nearestHome(Graph *g, int fromNode, bool *czyDom) {
    short *kolor = new short[g->getNodesNum()];
    int *odleglosc = new int[g->getNodesNum()];

    for (unsigned int i = 0; i < g->getNodesNum(); i++) {
        kolor[i] = BIALY;
        odleglosc[i] = 0;
    }

    FIFO <int> f(g->getNodesNum() + 10);

    kolor[fromNode] = SZARY;
    f.enqueue(fromNode);
    
    int ret = 2147483647;
    
    while (!f.empty()) {
        int u = f.dequeue();

        if(czyDom[u] && u != fromNode) {
            ret = odleglosc[u];
            break;
        }
        
        Edge *tmp = g->nodes[u];
        while (tmp != NULL) {
            if (kolor[tmp->toNode] == BIALY) {
                odleglosc[tmp->toNode] = odleglosc[u] + 1;

                    kolor[tmp->toNode] = SZARY;
                    f.enqueue(tmp->toNode);
            }

            tmp = tmp->next;
        }

        kolor[u] = CZARNY;
    }

    delete[] kolor;
    delete[] odleglosc;
    
    return ret;
}


int performTest(Graph *g, int *domy, bool *czyDom, int iloscDomow) {

    int min = bfs_nearestHome(g, domy[0], czyDom);

    for (int i = 1; i < iloscDomow; i++) {

        if(min <= 1)
            break;

        int tmp = bfs_nearestHome(g, domy[i], czyDom);
        if(min > tmp)
            min = tmp;
    }
    
    return min;
}

int main (int argc, const char * argv[]) {

    unsigned int iloscMiast, iloscWezlow, iloscKrawedzi;

    fscanf(stdin, "%u", &iloscMiast);

    while (iloscMiast--) {
        eMemCounter = 0;
        fscanf(stdin, "%u %u", &iloscWezlow, &iloscKrawedzi);

        Graph *graph = new Graph(iloscWezlow);

        while (iloscKrawedzi--) {
            unsigned int fromNode, toNode;
            fscanf(stdin, "%u %u", &fromNode, &toNode);

            graph->addEdge(fromNode, toNode);
        }

        unsigned int iloscDomow;
        fscanf(stdin, "%u", &iloscDomow);

        int *domy = new int [iloscDomow];
        
        bool *czyDom = new bool[iloscWezlow];
        for (unsigned int i = 0; i < iloscWezlow; i++)
            czyDom[i] = false;
        
        for (unsigned int i = 0; i < iloscDomow; i++) {
            int tmp;
            fscanf(stdin, "%u", &tmp);
            domy[i] = tmp;
            czyDom[tmp] = true;
        }
        
        int min = performTest(graph, domy, czyDom, iloscDomow);

        fprintf(stdout, "%d\n", min);

        delete graph;
        delete[] domy;
        delete[] czyDom;
    }

    return 0;
}
