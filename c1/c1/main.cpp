//
//  main.cpp
//  cw_4
//
//  Created by Oskar Gargas on 27.10.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

#define maxV 10000000
#define maxE 10000000

//13B * maxE = ~124MiB
struct Edge {
    int toNode;
    bool visited;
    Edge *next;
};

//24B * maxV = ~230MiB
class Node {
    int number;
    int weight;
    Edge *head;
    Edge *tail;

    public:
    Node() {
        head = NULL;
        tail = NULL;
        weight = 0;
    }

    void clean() {
        head = NULL;
        tail = NULL;
        weight = 0;
    }

    void addEdge(int toNode);

    int edgeAtNLeadsTo(int n);

    Edge *edgeAt(int n);
//    void printAll();

    int getWeight() {
        return weight;
    }

    int getNumber() {
        return number;
    }

    void setNumber(int n) {
        number = n;
    }
};

class Graph {

    public:
    Graph(int numberOfNodes) {
        while (numberOfNodes--)
            addNode();
    }

    void addNode();

    void addEdge(int from, int to);

    unsigned int numberOfNodes();
//    void printAll();
};

Node nMemPool[maxV];
unsigned int nMemPoolCounter = 0;

Edge eMemPool[maxE];
unsigned int eMemPoolCounter = 0;

int main(int argc, const char *argv[]) {
    int testNumber, nodeNumber, edgeNumber;
    int from, to;
    fscanf(stdin, "%d", &testNumber);

    while (testNumber--) {
        nMemPoolCounter = 0;
        eMemPoolCounter = 0;
        fscanf(stdin, "%d %d", &nodeNumber, &edgeNumber);
        if (nodeNumber >= edgeNumber) {
//      if(edgeNumber - nodeNumber != 1) {      //TODO: Moze to prawdziwe. Sprawdzic.
            for (int i = 0; i < edgeNumber; i++)
                fscanf(stdin, "%d %d", &from, &to);

            fprintf(stdout, "nie\n");
            continue;
        }

        if (edgeNumber < 10) {
            for (int i = 0; i < edgeNumber; i++)
                fscanf(stdin, "%d %d", &from, &to);

            fprintf(stdout, "nie\n");
            continue;
        }

        //Wczytać graf.
        Graph *g = new Graph(nodeNumber);
        for (int i = 0; i < edgeNumber; i++) {
            fscanf(stdin, "%d %d", &from, &to);
            g->addEdge(from, to);
        }

//        fprintf(stdout, "nodeNumber = %d, edgeNumber = %d\n", nodeNumber, edgeNumber);
//        g->printAll();

        //Sprawdzic czy ma:
        //  dokładnie dwa wierzchołki o stopniu 1 - to końce czułek;
        //  dokładnie jeden wierzchołek o stopniu 5 - to głowa motyla;
        //  dokładnie jeden wierzchołek o stopniu 3 - to tyłek motyla;
        //oraz zapisać te wierzchołki na później.
        int nW1_1 = -1, nW1_2 = -1, nW3 = -1, nW5 = -1, nW2c = 0;
        bool throwOut = false;

        for (unsigned int i = 0; i < g->numberOfNodes(); i++) {

            switch (nMemPool[i].getWeight()) {
                case 1:
                    if (nW1_1 == -1)
                        nW1_1 = i;
                    else if (nW1_2 == -1)
                        nW1_2 = i;
                    else
                        throwOut = true;
                    break;

                case 2:
                    nW2c++;
                    break;

                case 3:
                    if (nW3 == -1)
                        nW3 = i;
                    else
                        throwOut = true;
                    break;


                case 5:
                    if (nW5 == -1)
                        nW5 = i;
                    else
                        throwOut = true;
                    break;

                default:
                    throwOut = true;
                    break;
            }

            if (throwOut)
                break;
        }

        if (nW1_1 == -1 || nW1_2 == -1 || nW3 == -1 || nW5 == -1 || nW2c + 4 != nodeNumber)
            throwOut = true;

        if (throwOut) {
            fprintf(stdout, "nie\n");
            continue;
        }


        //Clean edition start

        //Sprawdzić, czy graf jest spójny.

        //Sprawdzić, czy czułki są równej długości.

        //Sprawdzić, czy tułów jest dłuższy od czułek.

        //Sprawdzić, czy skrzydła są równej długości i są dłuższe od tułowia.

        //Clean edition stop

        //BRUTE FORS START

        //Stań w głowie:
        Node *head = &nMemPool[nW5];
        Node *current;

        //  Dla każdej krawędzi rozpocznij wędrówkę do końca.
        //  Jak trafisz na tyłek to zapisz długość. x3
        //  Jak trafisz na wagę 1 -> zapisz długość czułka.
        //  Zapisuj ile krawędzi i wierzchołków odwiedziłeś
        //      Nie zapisz tyłka więcej niż 1 raz.
        int vNodes = 1, vEdges = 0;
        int lDoCzulka_1 = 0, lDoCzulka_2 = 0, lDoTylka_1 = 0, lDoTylka_2 = 0, lDoTylka_3 = 0;

        for (int i = 0; i < 5; i++) {
//            fprintf(stdout, "head leads to %d\n", head->edgeAtNLeadsTo(i));
            int lenght = 0;

            head->edgeAt(i)->visited = true;
            current = &nMemPool[head->edgeAtNLeadsTo(i)];
            lenght++;
            vEdges++;
            vNodes++;
            if (current->edgeAt(0)->toNode == head->getNumber())
                current->edgeAt(0)->visited = true;
            else
                current->edgeAt(1)->visited = true;

            while (current->getWeight() == 2) {
                int tmpNumber = current->getNumber();

                if (current->edgeAt(0)->visited == false) {
                    current->edgeAt(0)->visited = true;
                    current = &nMemPool[current->edgeAtNLeadsTo(0)];
                } else {
                    current->edgeAt(1)->visited = true;
                    current = &nMemPool[current->edgeAtNLeadsTo(1)];
                }

                if (current->edgeAt(0)->toNode == tmpNumber)
                    current->edgeAt(0)->visited = true;
                else
                    current->edgeAt(1)->visited = true;

                lenght++;
                vEdges++;
                vNodes++;
            }

//            fprintf(stdout, "curr weight = %d\n", current->getWeight());

            switch (current->getWeight()) {
                case 1:
                    if (lDoCzulka_1 == 0)
                        lDoCzulka_1 = lenght;
                    else if (lDoCzulka_2 == 0)
                        lDoCzulka_2 = lenght;
                    else
                        throwOut = true;
                    break;

                case 3:
                    if (lDoTylka_1 == 0)
                        lDoTylka_1 = lenght;
                    else if (lDoTylka_2 == 0)
                        lDoTylka_2 = lenght;
                    else if (lDoTylka_3 == 0)
                        lDoTylka_3 = lenght;
                    else
                        throwOut = true;
                    break;

                default:
                    throwOut = true;
                    break;
            }

            if (throwOut)
                break;
        }

        //  Porównaj liczbę odwiedzonych wierzchołków z nodeNumber i liczbę odwiedzonych krawędzi z edgeNumber.
        //      Jeśli są równe to graf jest spójny.
        vNodes -= 2;
//        fprintf(stdout, "vNodes = %d, vEdges = %d\n", vNodes, vEdges);
        if (vNodes != nodeNumber || vEdges != edgeNumber)
            throwOut = true;

//        fprintf(stdout, "lDoCzulka_1 = %d, lDoCzulka_2 = %d, lDoTylka_1 = %d, lDoTylka_2 = %d, lDoTylka_3 = %d\n",
//                lDoCzulka_1, lDoCzulka_2, lDoTylka_1, lDoTylka_2, lDoTylka_3);

        //  Porównaj długość czułków.
        if (lDoCzulka_1 != lDoCzulka_2)
            throwOut = true;

        //  Sprawdź, czy cykle z tyłkiem są dłuższe od długości czułków.
        if (lDoTylka_1 <= lDoCzulka_1 || lDoTylka_2 <= lDoCzulka_1 || lDoTylka_3 <= lDoCzulka_1)
            throwOut = true;

        if (throwOut) {
            fprintf(stdout, "nie\n");
            continue;
        }

        //  Sprawdź, czy są tam skrzydła o równej długości.
        throwOut = true;
        if (lDoTylka_1 == lDoTylka_2 && lDoTylka_3 < lDoTylka_1)
            throwOut = false;
        else if (lDoTylka_1 == lDoTylka_3 && lDoTylka_2 < lDoTylka_1)
            throwOut = false;
        else if (lDoTylka_2 == lDoTylka_3 && lDoTylka_1 < lDoTylka_2)
            throwOut = false;

        if (throwOut) {
            fprintf(stdout, "nie\n");
            continue;
        }

        //BRUTE FORS STOP

        //Jeśli nic wcześniej nie przerwało pętli to:
        fprintf(stdout, "tak\n");
        delete g;
    }

    return 0;
}

void Node::addEdge(int toNode) {
    Edge *e = &(eMemPool[eMemPoolCounter++]);
    e->toNode = toNode;
    e->visited = false;
    e->next = NULL;

    if (!weight)
        head = e;
    else
        tail->next = e;

    tail = e;
    weight++;
}

//void Node::printAll() {
//    fprintf(stdout, "[ weight = %d", weight);
//    Edge *tmp = head;
//    while (tmp != NULL) {
//        fprintf(stdout, ", toNode = %d", tmp->toNode);
//        tmp = tmp->next;
//    }
//    fprintf(stdout, " ]");
//}

int Node::edgeAtNLeadsTo(int n) {
    Edge *tmp = head;
    int it = 0;

    while (tmp != NULL && it != n) {
        tmp = tmp->next;
        it++;
    }

    return tmp->toNode;
}

Edge *Node::edgeAt(int n) {
    Edge *tmp = head;
    int it = 0;

    while (tmp != NULL && it != n) {
        tmp = tmp->next;
        it++;
    }

    return tmp;
}

void Graph::addNode() {
    nMemPool[nMemPoolCounter].clean();
    nMemPool[nMemPoolCounter].setNumber(nMemPoolCounter);
    nMemPoolCounter++;
}

unsigned int Graph::numberOfNodes() {
    return nMemPoolCounter;
}

void Graph::addEdge(int from, int to) {
    nMemPool[from].addEdge(to);
    nMemPool[to].addEdge(from);
}

//void Graph::printAll() {
//    fprintf(stdout, "[ numberOfNodes = %d", numberOfNodes());
//    for (unsigned int i = 0; i < numberOfNodes(); i++) {
//        fprintf(stdout, ",\n    ");
//        nMemPool[i].printAll();
//    }
//    fprintf(stdout, "\n]\n");
//}
