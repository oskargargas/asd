//
//  main.cpp
//  c0
//
//  Created by Oskar Gargas on 22.10.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

int n, k;

struct Klient {
    int nrKonta, nrKontaDocelowego;
    unsigned int kwotaPrzelewu;
    unsigned long long srodkiNaKoncie;
    bool zmianaNaLepsze;    
};

void swap(Klient &a, Klient &b);

class Pok {
    
public:
    Pok(){
        tabSize = 2 + (n + (k - n % k)) / k + n % k;
        tab = new Klient[tabSize];
        liczbaElementow = 0;
    }
    
    ~Pok() {
        delete[] tab;
    }
    
    void push(Klient k);
    Klient pop();    
    void update(int nrKontaDocelowego, unsigned long long kwotaPrzelewu, unsigned int &klienciNaLepsze, unsigned int &sumaZmianNaLepsze);
    
private:
    Klient *tab;
    int liczbaElementow, tabSize;
    
    int upHeap();    
    int upHeap(int pozycja);
    void downHeap();
};


void Pok::push(Klient k) {
    tab[++liczbaElementow] = k;
    upHeap();
}

Klient Pok::pop() {
    Klient ret;
    ret.nrKonta = -1;
    
    if (!liczbaElementow)
        return ret;
    
    ret = tab[1];
    tab[1].srodkiNaKoncie = 0;
    tab[1].nrKonta = -1;
    downHeap();
    liczbaElementow--;
    
    return ret;
}

void Pok::update(int nrKontaDocelowego, unsigned long long kwotaPrzelewu, unsigned int &klienciNaLepsze, unsigned int &sumaZmianNaLepsze) {
    int pozycja = -1;
    
    for (int i = 1; i <= liczbaElementow; i++) {
        if (tab[i].nrKonta == nrKontaDocelowego) {
            pozycja = i;
            break;
        }
    }
    
    if (pozycja == -1)
        return;
    
    bool czyByloLepiej = tab[pozycja].srodkiNaKoncie > tab[pozycja xor 1].srodkiNaKoncie || 
    (tab[pozycja].srodkiNaKoncie == tab[pozycja xor 1].srodkiNaKoncie && tab[pozycja].nrKonta < tab[pozycja xor 1].nrKonta);
    
    tab[pozycja].srodkiNaKoncie += kwotaPrzelewu;
    int nowaPozycja = upHeap(pozycja);
    
    bool czyJestLepiej = (tab[nowaPozycja].srodkiNaKoncie > tab[nowaPozycja xor 1].srodkiNaKoncie) || 
    (tab[nowaPozycja].srodkiNaKoncie == tab[nowaPozycja xor 1].srodkiNaKoncie && tab[pozycja].nrKonta < tab[pozycja xor 1].nrKonta);
    
    if (nowaPozycja != pozycja) {
        sumaZmianNaLepsze++;
        if (!tab[nowaPozycja].zmianaNaLepsze) {
            tab[nowaPozycja].zmianaNaLepsze = true;
            klienciNaLepsze++;
        }
    } else if (!czyByloLepiej && czyJestLepiej) {
        sumaZmianNaLepsze++;
        if (!tab[nowaPozycja].zmianaNaLepsze) {
            tab[pozycja].zmianaNaLepsze = true;
            klienciNaLepsze++;
        }
    }
}

int Pok::upHeap() {
    return upHeap(liczbaElementow);
}

int Pok::upHeap(int pozycja) {
    while (tab[pozycja].srodkiNaKoncie > tab[pozycja >> 1].srodkiNaKoncie && pozycja > 1 && pozycja > 1) {
        swap(tab[pozycja], tab[pozycja >> 1]);
        pozycja = pozycja >> 1;
    }
    return pozycja;
}

void Pok::downHeap() {
    int pozycja = 1;
    int doZamiany = 0;
    
    do {
        doZamiany = tab[pozycja << 1].srodkiNaKoncie > tab[(pozycja << 1) + 1].srodkiNaKoncie ? 
        pozycja << 1 : (pozycja << 1) + 1;
        
        if (tab[pozycja << 1].srodkiNaKoncie == tab[(pozycja << 1) + 1].srodkiNaKoncie)
            doZamiany = tab[pozycja << 1].nrKonta < tab[(pozycja << 1) + 1].nrKonta ? 
            pozycja << 1 : (pozycja << 1) + 1;
        
        if (tab[doZamiany].srodkiNaKoncie > tab[pozycja].srodkiNaKoncie || tab[pozycja].nrKonta == -1) {
            swap(tab[pozycja], tab[doZamiany]);
            pozycja = doZamiany;
        } else {
            doZamiany = 0;
        }            
    } while (doZamiany && pozycja < liczbaElementow);
    
}

void swap(Klient &a, Klient &b) {
    Klient tmp = a;
    a = b;
    b = tmp;
}

int main(int argc, char *argv[]) {
    
    scanf("%u %u", &n, &k);
    
    unsigned int zrealizowanePrzelewy = 0,
    odrzuconePrzelewy = 0,
    klienciNaLepsze = 0,
    sumaZmianNaLepsze = 0;
    
    Pok *poks = new Pok[k];
    
    for (int i = 0; i < n; i++) {
        Klient kl;
        kl.nrKonta = i;
        kl.zmianaNaLepsze = false;
        scanf("%llu %u %u",
              &(kl.srodkiNaKoncie), &(kl.nrKontaDocelowego), &(kl.kwotaPrzelewu));
        poks[kl.nrKonta % k].push(kl);
    }
    
    //pop all
//    for (int i = 0; i < n + 10; i++) {
//        Klient kl = poks[i % k].pop();
//        printf("{ nrKonta = %d,nrKontaDoc = %d, kwotaPrzelewu = %u, srodkiNaKoncie = %llu }\n", 
//               kl.nrKonta, kl.nrKontaDocelowego, kl.kwotaPrzelewu, kl.srodkiNaKoncie);
//    }
    
    for (int i = 0; i < n + 10; i++) {
        Klient kl = poks[i % k].pop();
        
        if (kl.nrKonta == -1)
            continue;
        
        if (kl.srodkiNaKoncie < kl.kwotaPrzelewu) {
            odrzuconePrzelewy++;
            continue;
        }
        
        zrealizowanePrzelewy++;
        poks[kl.nrKontaDocelowego % k].update(kl.nrKontaDocelowego, kl.kwotaPrzelewu, klienciNaLepsze, sumaZmianNaLepsze);
    }
    
    printf("%u %u %u %u\n", zrealizowanePrzelewy, odrzuconePrzelewy, klienciNaLepsze, sumaZmianNaLepsze);
    
	return 0;
}
