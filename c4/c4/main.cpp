//
//  main.cpp
//  c4
//
//  Created by Oskar Gargas on 17.12.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

//define OG_DEBUG

#ifdef OG_DEBUG
#import <cstdlib>
#endif

const unsigned long long infinity = 18446744073709551615;

struct Edge {
    unsigned int toNode;
    unsigned int weight;
    Edge *next;

    Edge() {
        toNode = 0;
        weight = 0;
        next = NULL;
    }

    #ifdef OG_DEBUG
    char * printEdge(unsigned int fromNode) {
        char *ret = NULL;
        asprintf(&ret, "%u -- %u [ label = \"%u\" ];\n", fromNode, toNode, weight);
        return ret;
    }
    #endif
};

#define eMaxVal 8000000
Edge eMemPool[eMaxVal];
unsigned int eMemCounter = 0;

struct Pair {
    int index;
    unsigned long long weight;

    void operator=(Pair const tmp) {
        index = tmp.index;
        weight = tmp.weight;
    }
    
    bool operator<(Pair const tmp) {
        return weight < tmp.weight;
    }

    bool operator>(Pair const tmp) {
        return weight > tmp.weight;
    }
};

class PrQueue {
    public:
    PrQueue() {
        elements = 1;
        size = 2000001; 
        heapTable = new Pair[size];
        whereIsIndex = new int[size];
    }
    
    PrQueue(unsigned int ull_size) {
        elements = 1;
        size = ull_size + 5;
        heapTable = new Pair[size];
        whereIsIndex = new int[size];
    }
    
    ~PrQueue() {
        delete[] heapTable;
    }
    
    void push(Pair data) {
        heapTable[elements] = data;
        whereIsIndex[data.index] = elements;
        upHeap(elements++);
    }
    
    Pair pop() {
        Pair ret;
        ret.index = -1;
        ret.weight = 0;
        
        if (!isEmpty()) {
            ret = heapTable[1];
            heapTable[1] = heapTable[--elements];
            downHeap(1);
        }

        return ret;
    }
    
    void update(Pair data) {
        if (heapTable[whereIsIndex[data.index]].index != data.index)
            fprintf(stderr, "Update fucken error!\n");
        
        heapTable[whereIsIndex[data.index]].weight = data.weight;
        upHeap(whereIsIndex[data.index]);
    }
    
    bool isEmpty() {
        return !(elements - 1);
    }
    
    private:
    Pair *heapTable;
    int *whereIsIndex;
    int elements;
    unsigned int size;

    void swapInHeap(int a, int b) {
        whereIsIndex[heapTable[a].index] = b;
        whereIsIndex[heapTable[b].index] = a;
        
        Pair tmp = heapTable[a];
        heapTable[a] = heapTable[b];
        heapTable[b] = tmp;
    }
    
    void upHeap(int index) {
        if (index > 1) {
            int parent = index >> 1;
            if (heapTable[index] < heapTable[parent]) {
                swapInHeap(index, parent);
                upHeap(parent);
            }
        }
    }

    void downHeap(int index) {
        
        if (index < elements - 1) {
            int l = index << 1;
            int p = (index << 1) + 1 ;
            
            if (heapTable[l] < heapTable[index]) {
                if (heapTable[p] < heapTable[l]) {
                    swapInHeap(index, p);
                    downHeap(p);
                } else {
                    swapInHeap(index, l);
                    downHeap(l);
                }
            } else if (heapTable[p] < heapTable[index]) {
                swapInHeap(index, p);
                downHeap(p);
            }
        }
    }
};

class Graph {
    public:
    Graph(unsigned int nodesNumber) {
        this->nodesNum = nodesNumber;
        this->nodes = new Edge*[nodesNum];

        for (unsigned int i = 0; i < nodesNumber; i++)
            this->nodes[i] = NULL;
    }

    ~Graph() {
        delete[] this->nodes;
    }

    void addEdge(unsigned int fromNode, unsigned int toNode, unsigned int weight);
    
    inline unsigned long long routeLenght(int fromNode, int toNode);
    
    inline unsigned long long *getRoutesTable(int fromNode);

    unsigned long long przetworzZapytania(int *zapytania, int iloscZapytan);

    unsigned int getNodesNum() {
        return nodesNum;
    }

#ifdef OG_DEBUG
    const char* printGraph();
#endif

    private:
    unsigned int nodesNum;
    Edge **nodes;

    unsigned long long *getRoutesTable(int fromNode, int toNode);

    int getMostFrequent(int *zapytania, int iloscZapytan);

};

void Graph::addEdge(unsigned int fromNode, unsigned int toNode, unsigned int weight) {
    Edge *e = &eMemPool[eMemCounter++];

    e->toNode = toNode;
    e->weight = weight;
    e->next = nodes[fromNode];
    nodes[fromNode] = e;

    e = &eMemPool[eMemCounter++];

    e->toNode = fromNode;
    e->weight = weight;
    e->next = nodes[toNode];
    nodes[toNode] = e;
}

inline unsigned long long Graph::routeLenght(int fromNode, int toNode) {
    unsigned long long *odleglosc = getRoutesTable(fromNode, toNode);
    unsigned long long ret = odleglosc[toNode];

    delete[] odleglosc;

    return ret;
}

inline unsigned long long *Graph::getRoutesTable(int fromNode) {
    return getRoutesTable(fromNode, -1);
}

unsigned long long *Graph::getRoutesTable(int fromNode, int toNode) {
    int *rodzice = new int[nodesNum];
    unsigned long long *odleglosc = new unsigned long long[nodesNum];
//    bool *isVisited = new bool[nodesNum];

    PrQueue q(nodesNum);

    Pair p;
    
    //Inicjalizacja
    for (int i = 0; i < nodesNum; i++) {
        rodzice[i] = -1;
        odleglosc[i] = infinity;
//        isVisited[i] = false;
        p.index = i;
        p.weight = infinity;
        q.push(p);
    }
    odleglosc[fromNode] = 0;
    p.index = fromNode;
    p.weight = 0;
    q.update(p);

//    int indexMin = fromNode;
    int indexMin;
//    unsigned long long min = odleglosc[fromNode];

    for (int i = 0; i < nodesNum; i++) {

        //TODO: Użyć kolejki priorytetowej (binary heap)
        //getIndexMin
//        for (int j = 0; j < nodesNum; j++) {
//            if (min > odleglosc[j] && !isVisited[j]) {
//                min = odleglosc[j];
//                indexMin = j;
//            }
//        }
        ////getIndexMin

        Pair p2 = q.pop();
        indexMin = p2.index;
//        fprintf(stderr, "index - %d\n", indexMin == p2.index);
//        fprintf(stderr, "weight - %d\n", min == p2.weight);
//        if (min != p2.weight) {
//            fprintf(stderr, "i. %d - %d\n", indexMin, p2.index);
//            fprintf(stderr, "w. %d - %d\n", min, p2.weight);
//        }

        Edge *edge = nodes[p2.index];
//        isVisited[indexMin] = true;

        while (edge != NULL) {
            if (odleglosc[indexMin] + edge->weight < odleglosc[edge->toNode]) {
                odleglosc[edge->toNode] = odleglosc[indexMin] + edge->weight;
                p.index = edge->toNode;
                p.weight = odleglosc[edge->toNode];
                q.update(p);
                rodzice[edge->toNode] = indexMin;
            }

            edge = edge->next;
        }

        if (indexMin == toNode)
            break;

//        min = infinity;
    }

    delete[] rodzice;
//    delete[] isVisited;

    return odleglosc;
}

unsigned long long Graph::przetworzZapytania(int *zapytania, int iloscZapytan) {
    int mostFrequentNode = getMostFrequent(zapytania, iloscZapytan);
    unsigned long long *mostFrequentTable = getRoutesTable(mostFrequentNode);
    
    int fromNode = zapytania[0], lastNode = zapytania[0];
    unsigned long long dlugoscMarszruty = 0;

    //Sprawdzamy dlugosc marszruty po swiatlowodach
    for (int i = 1; i < iloscZapytan; i++) {
        if (lastNode != zapytania[i]) {
            
            if (lastNode == mostFrequentNode)
                dlugoscMarszruty += mostFrequentTable[zapytania[i]];
            else if (zapytania[i] == mostFrequentNode)
                dlugoscMarszruty += mostFrequentTable[lastNode];
            else
                dlugoscMarszruty += routeLenght(lastNode, zapytania[i]);

            lastNode = zapytania[i];
        }
    }

    if (lastNode == mostFrequentNode)
        dlugoscMarszruty += mostFrequentTable[fromNode];
    else if (fromNode == mostFrequentNode)
        dlugoscMarszruty += mostFrequentTable[lastNode];
    else
        dlugoscMarszruty += routeLenght(lastNode, fromNode);

    delete[] mostFrequentTable;
    
    return dlugoscMarszruty;
}

int Graph::getMostFrequent(int *zapytania, int iloscZapytan) {
    int *frequencyTable = new int[nodesNum];

    for (int i = 0; i < nodesNum; i++)
        frequencyTable[i] = 0;

    frequencyTable[zapytania[0]]++;
    for (int i = 1; i < iloscZapytan; i++)
        if (zapytania[i - 1] != zapytania[i])
            frequencyTable[zapytania[i]]++;
    
    int indexMax = 0;
    int max = frequencyTable[0];

    //getIndexMax
    for (int i = 1; i < iloscZapytan; i++) {
        if (frequencyTable[i] > max) {
            max = frequencyTable[i];
            indexMax = i;
        }
    }
    //getIndexMax
    
    delete[] frequencyTable;
    return indexMax;
}
        
#ifdef OG_DEBUG
const char* Graph::printGraph() {
    char *ret = "graph graf {\nsplines = true;\n";

    for (unsigned int i = 0; i < nodesNum; i++) {
        Edge *tmp = nodes[i];
        while (tmp != NULL) {
            asprintf(&ret, "%s%s", ret, tmp->printEdge(i));
            tmp = tmp->next;
        }
    }

    asprintf(&ret, "%s}\n", ret);

    FILE *f = fopen("/tmp/state.dot", "w");
    fprintf(f, "%s", ret);
    fclose(f);

    system("/opt/local/bin/neato -Tsvg -o /tmp/state.svg /tmp/state.dot && open /tmp/state.svg");

    return ret;
}

const char* printGraph(Graph *g) {
    return g->printGraph();
}
#endif

unsigned int find(unsigned int *forest, unsigned int node) {
    unsigned int root = node;

    while (root != forest[root])
            root = forest[root];

    if (node != root || forest[node] != root) {
        unsigned int tmp = node;
        unsigned int tmp2;

        while (forest[tmp] != tmp) {
            tmp2 = forest[tmp];
            forest[tmp] = root;
            tmp = tmp2;
        }
    }

    return forest[node];
}

void link(unsigned int *cityForest, unsigned int a, unsigned int b) {
    cityForest[b] = a;
}

void unionSets (unsigned int *cityForest, unsigned int a, unsigned int b) {
    link(cityForest, find(cityForest, a), find(cityForest, b));
}

bool doIWantToAddThisEdge(unsigned int *cityForest, unsigned int fromNode, unsigned int toNode) {
    bool ret = true;

    if (find(cityForest, fromNode) == find(cityForest, toNode))
        ret = false;
    else
        unionSets(cityForest, fromNode, toNode);

    return ret;
}

int main (int argc, const char * argv[]) {

    unsigned int iloscMiast, iloscDomow;
    unsigned long long iloscPolaczen;

    fscanf(stdin, "%u", &iloscMiast);

    while (iloscMiast--) {
        eMemCounter = 0;
        fscanf(stdin, "%u %llu", &iloscDomow, &iloscPolaczen);

        unsigned int *domy = new unsigned int[iloscDomow];
        for (unsigned int i = 0; i < iloscDomow; i++)
            domy[i] = i;

        Graph *mst = new Graph(iloscDomow);
        Graph *full = new Graph(iloscDomow);

        while (iloscPolaczen--) {
            unsigned int fromNode, toNode, weight;
            fscanf(stdin, "%u %u %u", &fromNode, &toNode, &weight);

            full->addEdge(fromNode, toNode, weight);

            if(doIWantToAddThisEdge(domy, fromNode, toNode))
                mst->addEdge(fromNode, toNode, weight);
        }

//        full->printGraph();

        int iloscZapytan = 0;
        fscanf(stdin, "%d", &iloscZapytan);

        int *zapytania = new int[iloscZapytan];
        for (int i = 0; i < iloscZapytan; i++) {
            int tmp;
            fscanf(stdin, "%d", &tmp);

            zapytania[i] = tmp;
        }

        delete[] domy;

        fprintf(stdout, "%llu ", mst->przetworzZapytania(zapytania, iloscZapytan));
        delete mst;

        fprintf(stdout, "%llu\n", full->przetworzZapytania(zapytania, iloscZapytan));
        delete full;

        delete[] zapytania;
    }

    return 0;
}
