define graph
  if $argc != 1
    echo Usage: graph variable\n
  else
    printf "%s\n", printGraph($arg0)
    set pagination off
    set print elements 0
    set logging off
    set logging file /tmp/state.dot
    set logging overwrite on
    set logging redirect on
    set logging on
    printf "%s\n", printGraph($arg0)
    set logging off
    shell neato -Tsvg -o /tmp/state.svg /tmp/state.dot && open /tmp/state.svg
    set pagination on
  end
end
