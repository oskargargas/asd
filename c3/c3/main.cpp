//
//  main.cpp
//  c3
//
//  Created by Oskar Gargas on 07.12.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <cstdio>

#define BIALY 1
#define SZARY 2
#define CZARNY 3

#define QU_SIZE 1000000

template <class T>
class FIFO {
    public:
    FIFO() {
        elements = 0;
        tail = 0;
        head = 0;
    }

    void enqueue(T el) {
        q[tail++] = el;
        elements++;

        if(tail == QU_SIZE)
            tail = 0;
    }
    
    T dequeue() {
       T x = q[head++];
        elements--;

        if (head == QU_SIZE)
            head = 0;

        return x;
    }
    
    bool empty() {
        return !elements;
    }

    private:
    T q[QU_SIZE];
    unsigned int head;
    unsigned int tail;
    unsigned int elements;
};

struct Edge {
    unsigned int toNode;
    unsigned int weight;
    Edge *next;

    Edge() {
        toNode = 0;
        weight = 0;
        next = NULL;
    }
};

#define eMaxVal 2000000
Edge eMemPool[eMaxVal];
unsigned int eMemCounter = 0;

class Graph {
    public:
    Graph(unsigned int nodesNumber) {
        this->nodesNum = nodesNumber;
        this->sumaWag = 0;
        this->nodes = new Edge*[nodesNum];

        for (unsigned int i = 0; i < nodesNumber; i++)
            this->nodes[i] = NULL;
    }

    ~Graph() {
        delete[] this->nodes;
    }

    void addEdge(unsigned int fromNode, unsigned int toNode, unsigned int weight) {
        this->sumaWag += weight;

        Edge *e = &eMemPool[eMemCounter++];

        e->toNode = toNode;
        e->weight = weight;
        e->next = nodes[fromNode];
        nodes[fromNode] = e;

        e = &eMemPool[eMemCounter++];

        e->toNode = fromNode;
        e->weight = weight;
        e->next = nodes[toNode];
        nodes[toNode] = e;
    }

    unsigned int getSumaWag() {
        return sumaWag;
    }

    unsigned int getNodesNum() {
        return nodesNum;
    }

    unsigned int nodesNum;
    unsigned int sumaWag;
    Edge **nodes;

};

void performTest(Graph *g, unsigned int fromNode, unsigned int radius, unsigned int & retDomy, unsigned int & retKabel) {
    retDomy = 0;
    retKabel = 0;

    short *kolor = new short[g->getNodesNum()];
    unsigned long long *odleglosc = new unsigned long long[g->getNodesNum()];

    for (unsigned int i = 0; i < g->getNodesNum(); i++) {
        kolor[i] = BIALY;
        odleglosc[i] = 0;
    }

    kolor[fromNode] = SZARY;

    FIFO <unsigned int> f;
    f.enqueue(fromNode);
    
    while (!f.empty()) {
        unsigned int u = f.dequeue();

        retDomy++;

        Edge *tmp = g->nodes[u];
        while (tmp != NULL) {
            if (kolor[tmp->toNode] == BIALY) {
                odleglosc[tmp->toNode] = odleglosc[u] + 1;
                if (odleglosc[tmp->toNode] <= radius) {
                    kolor[tmp->toNode] = SZARY;
                    retKabel += tmp->weight;
                    f.enqueue(tmp->toNode);
                } else {
                    kolor[tmp->toNode] = CZARNY;
                }
            }

            tmp = tmp->next;
        }

        kolor[u] = CZARNY;
    }

    delete[] kolor;
    delete[] odleglosc;
}

unsigned int find(unsigned int *forest, unsigned int node) {
    unsigned int root = node;

    while (root != forest[root])
            root = forest[root];

    if (node != root || forest[node] != root) {
        unsigned int tmp = node;
        unsigned int tmp2;

        while (forest[tmp] != tmp) {
            tmp2 = forest[tmp];
            forest[tmp] = root;
            tmp = tmp2;
        }
    }

//    if (node != forest[node])
//        forest[node] = find(forest, forest[node]);
    
    return forest[node]; 
}

void link(unsigned int *cityForest, unsigned int a, unsigned int b) {
    cityForest[b] = a;
}

void unionSets (unsigned int *cityForest, unsigned int a, unsigned int b) {
    link(cityForest, find(cityForest, a), find(cityForest, b));
}

bool doIWantToAddThisEdge(unsigned int *cityForest, unsigned int fromNode, unsigned int toNode) {
    bool ret = 1;
    
    if (find(cityForest, fromNode) == find(cityForest, toNode))
        ret = false;
    else
        unionSets(cityForest, fromNode, toNode);
    
    return ret;
}

int main (int argc, const char * argv[]) {

    unsigned int iloscMiast, iloscDomow;
    unsigned long long iloscPolaczen;

    fscanf(stdin, "%u", &iloscMiast);

    while (iloscMiast--) {
        eMemCounter = 0;
        fscanf(stdin, "%u %llu", &iloscDomow, &iloscPolaczen);
        unsigned int iloscElektrowni = iloscDomow;

        unsigned int *domy = new unsigned int[iloscDomow];
        for (unsigned int i = 0; i < iloscDomow; i++)
            domy[i] = i;

        Graph *mst = new Graph(iloscDomow);

        while (iloscPolaczen--) {
            unsigned int fromNode, toNode, weight;
            fscanf(stdin, "%u %u %u", &fromNode, &toNode, &weight);

            if(doIWantToAddThisEdge(domy, fromNode, toNode)) {
                mst->addEdge(fromNode, toNode, weight);
                iloscElektrowni--;
            }
        }

        fprintf(stdout, "%d %d\n", iloscElektrowni, mst->getSumaWag());

        unsigned int liczbaZapytan;
        fscanf(stdin, "%u", &liczbaZapytan);
        while (liczbaZapytan--) {
            unsigned int fromNode, lenght;
            fscanf(stdin, "%u %u", &fromNode, &lenght);

            unsigned int testDomy = 0, testKabel = 0;
            performTest(mst, fromNode, lenght, testDomy, testKabel);
            fprintf(stdout, "%d %d\n", testDomy, testKabel);
        }

        delete mst;
        delete[] domy;
    }

    return 0;
}
