//
//  main.cpp
//  zadImpl
//
//  Created by Oskar Gargas on 08.12.2011.
//  Copyright (c) 2011 PJWSTK. All rights reserved.
//

#include <iostream>
#include <vector>

using namespace std;

class StackInt {
    public:
    StackInt() {
    }
    
    void push(int a) {
        stack.push_back(a);
    }
    
    int pop() {
        int ret = stack[stack.back()];
        stack.pop_back();
        return ret;
    }
    
    bool isEmpty() {
        return stack.empty();
    }
    
    private:
    vector<int> stack;
    
};


int calculate(int a, int b, char op) {
    int ret = 0;
    
    switch (op) {
        case '+': ret = a + b; break;
        case '-': ret = a - b; break;
        case '*': ret = a * b; break;
    }
    
    return ret;
}

int main (int argc, const char * argv[]) {
    char c;

    StackInt arg, opr;

    while (scanf("%c", &c) && c != '\n') {
        if (isdigit(c))
            arg.push(c - '0');
       
        int a, b;
        char op;
        
        switch (c) {
            case ')':
            
                a = arg.pop();
                b = arg.pop();
                op = (char)opr.pop();

                arg.push(calculate(a, b, op));

                break;
            
            case '+': ;
            case '-': ;
            case '*': opr.push(c); break;
            default: break;
        }


    }

    printf("%d\n", arg.pop());

    return 0;
}

